# Sample Text Generator

A simple text-generator app made with React and Webpack. Makes use of bootswatch, axios, and the [Dino Ipsum API](http://dinoipsum.herokuapp.com/)

Built following Traversy Media's [React Text Generator tutorial](https://www.youtube.com/watch?v=yU5DYccb77A)