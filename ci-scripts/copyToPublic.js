const { ncp: copy } = require('ncp');
const path = require('path');
const fs = require('fs');

const PUBLIC_DIR = path.join(__dirname, '..', 'public');

if (!fs.existsSync(PUBLIC_DIR)) {
  fs.mkdirSync(PUBLIC_DIR);
}

// Files and folders relative to the project.
copyFiles = () => {
  copy('build/', 'public/', (err) => {
    if (err) {
      console.error('Error!');
      console.error(err);
      throw new err;
    }

    console.log('Files found. Copying completed.');
  });
};

module.exports = copyFiles;

