const rmdir = require('rmdir');
const path = require('path');

const PUBLIC_DIR = path.join(__dirname, '..', 'public');

const deletion = (callback = () => {}) => {
  rmdir(PUBLIC_DIR, callback, function (err, dirs, files) {
    if (err) {
      console.error('An error occured during deletion.');
      console.error(err);
      throw new err;
    }
    console.dir('Deleted: ', dirs, files);
    console.log('all files are removed');
    callback();
  });
}

module.exports = deletion;
