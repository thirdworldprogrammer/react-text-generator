import React, { Component } from 'react';
import axios from 'axios';
import Output from './components/Output';
import Select from './components/Controls/Select';
import Text from './components/Controls/Text';
import RefreshButton from './components/Controls/RefreshButton';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paras: 4,
      format: 'html',
    };
    this.changeFormat = this.changeFormat.bind(this);
    this.getSampleText = this.getSampleText.bind(this);
    this.changeParas = this.changeParas.bind(this);
  }

  componentWillMount() {
    this.getSampleText();
  }

  getSampleText() {
    const { paras, format } = this.state;
    const API_URL = `http://dinoipsum.herokuapp.com/api/?format=${format}&paragraphs=${paras}`;
    axios.get(API_URL)
      .then((response) => {
        let textData = null;
        if (format === "json") {
          textData = JSON.stringify(response.data, null, 2);
        } else {
          textData = response.data;
        }
        this.setState({ text: textData }, () => {
          console.log(this.state);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  changeParas(evt, value) {
    evt.preventDefault();
    this.setState({ paras: value });
  }

  changeFormat(evt, value) {
    evt.preventDefault();
    this.setState({ format: value });
  }

  render() {
    return (
      <div className="App container">
        <h1 className="text-center">ReactJS Sample Text Generator</h1>
        <hr/>
        <div className="form-inline">
          <div className="form-group">
            <label>HTML, Plaintext - or JSON?</label>
            <Select value={ this.state.value } onChange={ this.changeFormat } />
          </div>
          <br />
          <div className="form-group">
            <label>Paragraphs:</label>
            <Text value={ this.state.paras } onChange={ this.changeParas } />
          </div>
          <br />
          <br />
          <RefreshButton onRefresh={ this.getSampleText } />
          <br />
          <br />
        </div>
        <Output value={ this.state.text } format={ this.state.format } />
      </div>
    );
  }
}

export default App;
