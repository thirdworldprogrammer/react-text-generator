import React, { Component } from 'react';

class Text extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(evt) {
    const { value } = evt.target;
    evt.preventDefault();
    this.props.onChange(evt, value);
  }

  render() {
    const onChange = this.onChange;
    return (
      <div className="output">
        <input type="number"
          className="form-control"
          value={ this.props.value }
          onChange={ onChange }
          />
      </div>
    );
  }
}

export default Text;
