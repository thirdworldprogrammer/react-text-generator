import React, { Component } from 'react';

class Select extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(evt) {
    const { value } = evt.target;
    evt.preventDefault();
    this.props.onChange(evt, value);
  }

  render() {
    const onChange = this.onChange;
    return (
      <div className="output">
        <select name="" id="" className="form-control" onChange={onChange}>
          <option value="html">HTML</option>
          <option value="text">Text</option>
          <option value="json">JSON</option>
        </select>
      </div>
    );
  }
}

export default Select;
