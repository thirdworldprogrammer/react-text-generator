import React from 'react';
import PropTypes from 'prop-types';

const RefreshButton = ({ onRefresh }) => (
  <button className="btn btn-primary" onClick={onRefresh}>Refresh</button>
);

RefreshButton.propTypes = {
  onRefresh: PropTypes.func.isRequired,
};

export default RefreshButton;